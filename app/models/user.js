var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Schema = mongoose.Schema;


var UserSchema = new Schema({
    name: String,
    role: String,
    username: {type: String, required: true, index:{unique: true}},
    password: {type: String, required: true, select: false}
});



UserSchema.pre('save', function (next) {
    var user = this;

    if(!user.isModified('password')) return next();

    bcrypt.hash(user.password, null, null, function (err, hash) {
        if(err) return next();

        user.password = hash;
        next();
    });

});
 // custom method to compare password. behind the scene we are using bcrypt
 // function compareSync

UserSchema.methods.comparePassword = function (password) {
    var user = this ;  

    // fist param is the pass that we will provide
    // second is saved in database
    return bcrypt.compareSync(password, user.password);

};
module.exports = mongoose.model('User', UserSchema);