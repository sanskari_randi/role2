angular.module("myApp", ['mainCtrl', 'authService', 'appRoutes', 'userCtrl', 'userService', 'ngRoute'])

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });